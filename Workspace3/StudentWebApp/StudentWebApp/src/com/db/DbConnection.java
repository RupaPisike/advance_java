package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {
	
	public static Connection getConnection() {
		
		Connection con = null;
		String url = "jdbc:mysql://localhost:3306/sqldb";
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, "root", "Rupa@1381634");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		
		return con;		
	}
	
}

