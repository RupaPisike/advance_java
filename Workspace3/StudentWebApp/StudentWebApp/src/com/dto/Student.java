package com.dto;

public class Student {
	private int studentId;
	private String studentName;
	private String branch;
	private String gender;
	private String emailId;
	private String password;
	
	public Student() {
		super();
	}

	public Student(int studentId, String studentName, String branch, String gender, String emailId, String password) {
		super();
		this.studentId = studentId;
		this.studentName = studentName;
		this.branch = branch;
		this.gender = gender;
		this.emailId = emailId;
		this.password = password;
	}

	public int getstudentId() {
		return studentId;
	}
	public void setstudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Student [studentId=" + studentId + ", studentName=" + studentName + ", branch=" +branch + ", gender=" + gender
				+ ", emailId=" + emailId + ", password=" + password + "]";
	}
}